using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtY : MonoBehaviour
{
    [SerializeField] Transform look;
    [SerializeField] float rotationSpeed;

    private void Update()
    {
        Vector3 lookSameY = new Vector3(look.position.x, transform.position.y, look.position.z);
        Quaternion q = Quaternion.LookRotation(lookSameY - transform.position, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, q, Time.deltaTime * rotationSpeed);
    }
}
