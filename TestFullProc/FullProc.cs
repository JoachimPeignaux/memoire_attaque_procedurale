using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullProc : MonoBehaviour, AnimationInterface
{
    [SerializeField] CurveTarget curveTarget;

    [Header("Attack")]
    [SerializeField] float anticipationDuration;
    [SerializeField] float attackDuration;
    [SerializeField] float recoverDuration;
    [SerializeField] Sword sword;

    [Space]
    [SerializeField] Transform idlePos;
    Vector3 A, C;
    float timer;
    enum PHASE { NONE, OPENING, ATTACK, RETURN }
    PHASE currentPhase;
    private bool checkSword;
    Transform player;
    bool isAttacking;

    public void Awake() {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        currentPhase = PHASE.NONE;
        sword = GetComponentInChildren<Sword>();
    }

    public void Update() {
        timer += Time.deltaTime;
        float t;

        if (timer < anticipationDuration)
        {
            if (currentPhase == PHASE.RETURN || currentPhase == PHASE.NONE)
            {
                curveTarget.SetAC(idlePos.position, A);
                curveTarget.MakeAttackPassingByTarget(curveTarget.origin.forward.normalized * ((idlePos.position - A).magnitude / 5f) + curveTarget.origin.position);
                currentPhase = PHASE.OPENING;
                checkSword = false;
            }
            t = timer / anticipationDuration;
            t = Mathf.Min(1, t* 1.33f);
        }
        else if (timer <= anticipationDuration + attackDuration)
        {
            if (currentPhase == PHASE.OPENING)
            {
                curveTarget.SetAC(A, C);
                curveTarget.MakeAttackPassingByTarget(player.position);
                currentPhase = PHASE.ATTACK;
                sword.WriteOldPointList();
                checkSword = true;
            }
            t = (timer - anticipationDuration) / attackDuration;
        }
        else if (timer < anticipationDuration + attackDuration + recoverDuration)
        {
            if (currentPhase == PHASE.ATTACK)
            {
                curveTarget.SetAC(C, idlePos.position);
                curveTarget.MakeAttackPassingByTarget(curveTarget.origin.forward.normalized * ((idlePos.position - C).magnitude / 5f) + curveTarget.origin.position);
                currentPhase = PHASE.RETURN;
                checkSword = false;
            }
            t = (timer - anticipationDuration - attackDuration) / recoverDuration;
            t = Mathf.Min(1, t * 1.33f);
        }
        else {
            isAttacking = false;
            t = 1;
        }

        curveTarget.SetPosition(t);
        curveTarget.SetRotation();

        if (checkSword && currentPhase == PHASE.ATTACK)
        {
            if (sword.CheckForCollision())
            {
                checkSword = false;
                Debug.Log("Player hitted");
                GameManager.Instance.EnemyHitPlayer();
                checkSword = false;
                PlayerController.Instance.TakeDamage();
            }
            sword.WriteOldPointList();
        }
    }

    public void Idle()
    {
        curveTarget.SetAC(idlePos.position, idlePos.position);
    }

    public bool IsAttacking()
    {
        return isAttacking;
    }

    public void NewAttack()
    {
        (Vector3, Vector3) startAndEnd = curveTarget.GetStartAndEndForNewAttack();
        A = startAndEnd.Item1;
        C = startAndEnd.Item2;
        curveTarget.SetAC(A, C);
        curveTarget.MakeAttackPassingByTarget(player.position);
        timer = 0;
        isAttacking = true;
    }

    public void Turn()
    {
        return;
    }

    public void Walk()
    {
        Idle();
    }

}
