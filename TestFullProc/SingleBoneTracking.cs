using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBoneTracking : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float maxAngle;
    [SerializeField] float speed;
    
    protected Quaternion startRotation;

    private void Awake()
    {
        startRotation = transform.rotation;
    }

    private void LateUpdate()
    {
        Vector3 towardObject = target.position - transform.position;
        towardObject = Vector3.RotateTowards(transform.parent.forward, towardObject, Mathf.Deg2Rad * maxAngle, 0);
        Quaternion targetRotation = Quaternion.LookRotation(towardObject, Vector3.up) * startRotation; //ajouter la rotation de base pour ajouter l'offset
        Quaternion boneRotation = Quaternion.Slerp(transform.rotation, targetRotation, 1 - Mathf.Exp(-speed * Time.deltaTime));
        transform.rotation = boneRotation;
    }
}
