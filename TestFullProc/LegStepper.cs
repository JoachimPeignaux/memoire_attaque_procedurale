using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class LegStepper : MonoBehaviour
{
    [SerializeField] Transform homeTransform;
    [SerializeField] float wantStepAtDistance;
    [SerializeField] float stepOvershootFraction;
    [SerializeField] float stepHeightFraction = 0.5f;
    [SerializeField] float stepDuration;

    static bool moving;

    IEnumerator MoveToHome()
    {
        moving = true;

        Quaternion startRot = transform.rotation;
        Vector3 startPoint = transform.position;

        Quaternion endRot = homeTransform.rotation;

        Vector3 towardHome = (homeTransform.position - transform.position);

        float overshootDistance = wantStepAtDistance * stepOvershootFraction;
        Vector3 overshootVector = towardHome * overshootDistance;

        overshootVector = Vector3.ProjectOnPlane(overshootVector, Vector3.up);
        Vector3 endPoint = homeTransform.position + overshootVector;

        Vector3 centerPoint = (startPoint + endPoint) / 2;
        centerPoint += homeTransform.up * Vector3.Distance(startPoint, endPoint) * stepHeightFraction;

        float timeElapsed = 0;

        do
        {
            timeElapsed += Time.deltaTime;
            endPoint = homeTransform.position + overshootVector;
            float normalizedTime = timeElapsed / stepDuration;
            normalizedTime = EasingCubic(normalizedTime);

            transform.position = Vector3.Lerp(
                Vector3.Lerp(startPoint, centerPoint, normalizedTime),
                Vector3.Lerp(centerPoint, endPoint, normalizedTime),
                normalizedTime
            );

            transform.rotation = Quaternion.Slerp(startRot, endRot, 1);

            yield return null;
        }
        while (timeElapsed < stepDuration);

        moving = false;
    }

    public void Update()
    {
        if (moving) return;

        float distFromHome = Vector3.Distance(transform.position, homeTransform.position);

        if (distFromHome > wantStepAtDistance)
        {
            StartCoroutine(MoveToHome());
        }
    }

    static float EasingCubic(float k)
    {
        if ((k *= 2f) < 1f) return 0.5f * k * k * k;
        return 0.5f * ((k -= 2f) * k * k + 2f);
    }
}
