using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public Transform[] PointList;
    private Vector3[] newPointList;
    private Vector3[] oldPointList;
    [SerializeField] LayerMask targetLayer;

    private void Start()
    {
        newPointList = new Vector3[PointList.Length];
        oldPointList = new Vector3[PointList.Length];
        for (int i = 0; i < PointList.Length; i++)
        {
            newPointList[i] = PointList[i].position;
            oldPointList[i] = newPointList[i];
        }
    }

    public void WriteOldPointList()
    {
        for (int i = 0; i < PointList.Length; i++)
        {
            oldPointList[i] = PointList[i].position;
        }
    }

    public bool CheckForCollision() {
        bool hasHit = false;

        for (int i = 0; i < PointList.Length; i++)
        {
            newPointList[i] = PointList[i].position;
        }

        bool touched;
        for (int i = 0; i < newPointList.Length; i++)
        {
            Vector3 trajectoire = (oldPointList[i] - newPointList[i]);
            touched = Physics.Raycast(newPointList[i], trajectoire, trajectoire.magnitude, targetLayer);
            Debug.DrawRay(newPointList[i], trajectoire, Color.red);
            if (touched)
            {
                hasHit = true;
            }
        }

        return hasHit;
    }
}
