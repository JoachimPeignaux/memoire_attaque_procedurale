using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] RectTransform scorePlus;
    [SerializeField] RectTransform scoreMinus;

    [SerializeField] Text scoreText;
    int score;
    string startScoreText;

    [SerializeField] float animDuration;
    [SerializeField] float yOffset;

    [SerializeField] int plusAmount;
    [SerializeField] int minusAmount;

    Vector2 plusStartPos;
    Vector2 minusStartPos;

    float plusTime;
    float minusTime;

    Text plusText;
    Text minusText;

    [SerializeField] GameObject tutorialText;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        plusStartPos = scorePlus.localPosition;
        minusStartPos = scoreMinus.localPosition;

        plusText = scorePlus.GetComponent<Text>();
        minusText = scoreMinus.GetComponent<Text>();

        plusTime = animDuration;
        minusTime = animDuration;

        startScoreText = scoreText.text;
        ResetScore();

        plusText.text = "+" + plusAmount.ToString();
        minusText.text = "-" + minusAmount.ToString();
    }

    public void HideTutorialText() {
        tutorialText.SetActive(false);
    }

    public void Update()
    {
        plusTime += Time.deltaTime;
        minusTime += Time.deltaTime;

        scorePlus.localPosition = Vector2.Lerp(plusStartPos, plusStartPos + new Vector2(0, yOffset), plusTime/animDuration);
        scoreMinus.localPosition = Vector2.Lerp(minusStartPos, minusStartPos + new Vector2(0, yOffset), minusTime/animDuration);

        plusText.color = new Color(plusText.color.r, plusText.color.g, plusText.color.b, Mathf.Lerp(1,0, plusTime/animDuration));
        minusText.color = new Color(minusText.color.r, minusText.color.g, minusText.color.b, Mathf.Lerp(1, 0, minusTime / animDuration));
    }

    public void ChangeScore(bool plus) {
        if (plus)
        {
            plusText.color = new Color(plusText.color.r, plusText.color.g, plusText.color.b, 1.0f);
            plusTime = 0.0f;
            score += plusAmount;
        }
        else
        {
            minusText.color = new Color(minusText.color.r, minusText.color.g, minusText.color.b, 1.0f);
            minusTime = 0.0f;
            score -= minusAmount;
        }
        scoreText.text = startScoreText + score.ToString();
    }

    public void ResetScore() {
        score = 0;
        scoreText.text = startScoreText + "0";
    }
}
