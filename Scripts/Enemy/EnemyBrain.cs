using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBrain : MonoBehaviour
{
    AnimationInterface animationManager;
    [SerializeField] Vector2 restBetweenAttackMinMax;
    [SerializeField] float atkRange;
    Transform player;
    [SerializeField] float timeForFullTurn;
    [SerializeField] float angleToStartAtk;
    [SerializeField] float walkSpeed;
    [SerializeField] int maxComboCount;
    [SerializeField] float comboStep;
    int combo;
    bool canCombo;

    enum State { ATTACK, REST, FREE };
    State state;

    void Awake()
    {
        state = State.FREE;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Start()
    {
        animationManager = GetComponent<AnimationInterface>();
        animationManager.Idle();
        combo = 0;
    }

    void Update()
    {
        switch (state)
        {
            case State.ATTACK:
                if (!animationManager.IsAttacking())
                {
                    Vector3 projectedPosition2 = new Vector3(player.position.x, transform.position.y, player.position.z);
                    Vector3 dirToPlayer2 = projectedPosition2 - transform.position;
                    int r = Random.Range(0, maxComboCount - combo + 2);
                    if (combo >= maxComboCount || dirToPlayer2.sqrMagnitude > atkRange * atkRange + ((combo != 0) ? comboStep : 0) || r== 0 || !canCombo)
                        StartCoroutine(Rest());
                    else
                        state = State.FREE;
                }
                else
                {
                    Vector3 projectedPosition2 = new Vector3(player.position.x, transform.position.y, player.position.z);
                    Vector3 dirToPlayer2 = projectedPosition2 - transform.position;

                    Quaternion lookPlayer = Quaternion.LookRotation(dirToPlayer2, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, lookPlayer, 360 * Time.deltaTime / (timeForFullTurn * 5));
                }
                break;

            case State.FREE:

                //turn if needed
                Vector3 dirToPlayer = GetDirToPlayer();
                if (Vector3.Angle(transform.forward, dirToPlayer) >= angleToStartAtk)
                {
                    Quaternion lookPlayer = Quaternion.LookRotation(dirToPlayer, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, lookPlayer, 360 * Time.deltaTime / timeForFullTurn);
                    animationManager.Turn();
                }
                else
                {
                    //check if can atk
                    if (dirToPlayer.sqrMagnitude <= atkRange * atkRange + ((combo!=0)? comboStep : 0))
                    {
                        //attack
                        if (combo != 0) {
                            transform.position += dirToPlayer.normalized * comboStep;
                        }
                        animationManager.NewAttack();
                        state = State.ATTACK;
                        canCombo = true;
                        combo++;
                    }
                    else
                    {
                        //get clsoer
                        animationManager.Walk();
                        transform.position += dirToPlayer.normalized * Time.deltaTime * walkSpeed;
                    }
                }
                break;
        }
    }

    IEnumerator Rest() {
        state = State.REST;
        animationManager.Idle();
        combo = 0;
        yield return new WaitForSeconds(Random.Range(restBetweenAttackMinMax.x, restBetweenAttackMinMax.y));
        state = State.FREE;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Vector3 previousPoint = new Vector3();
        for (int i = 0; i <= 360; i += 10) {
            Vector3 point = transform.position + new Vector3(Mathf.Cos(i * Mathf.Deg2Rad) * atkRange, 0 , Mathf.Sin(i * Mathf.Deg2Rad) * atkRange);
            if (i != 0) {
                Gizmos.DrawLine(previousPoint, point);
            }
            previousPoint = point;
        }
    }

    Vector3 GetDirToPlayer() {
        Vector3 projectedPosition = new Vector3(player.position.x, transform.position.y, player.position.z);
        return projectedPosition - transform.position;
    }

    public void Hitted() {
       // canCombo = false;
    }

}
