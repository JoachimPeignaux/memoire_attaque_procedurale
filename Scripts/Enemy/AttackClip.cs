using UnityEngine;

[CreateAssetMenu(fileName = "Atk", menuName = "ScriptableObjects/new AttackClip")]
public class AttackClip : ScriptableObject
{
    public AnimationClip animationClip;
    public float startAtk;
    public float endAtk;
}
