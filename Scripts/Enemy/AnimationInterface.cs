using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface AnimationInterface
{
    public void NewAttack();
    public bool IsAttacking();
    public void Idle();
    public void Turn();
    public void Walk();
}
