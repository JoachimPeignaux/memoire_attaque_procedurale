using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackZone : MonoBehaviour
{
    public AttackClip attackClip;
    public Collider zone;

    private void Awake()
    {
        zone.isTrigger = true;
    }
}
