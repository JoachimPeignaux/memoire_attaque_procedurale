using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicAnimation : MonoBehaviour, AnimationInterface
{
    [Header("Attacking")]
    Transform player;
    [SerializeField] AttackZone[] attackZones;
    [SerializeField] float attackRangeAcceptance;
    Sword sword;
    string currentAttack;
    bool checkSword;

    [Space]
    [SerializeField] AnimationClip idleClip;
    [SerializeField] float idleCrossFade;
    [SerializeField] AnimationClip turnClip;
    [SerializeField] float turnCrossFade;
    [SerializeField] AnimationClip walkClip;
    [SerializeField] float walkCrossFade;

    Animation animator;

    public void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        currentAttack = "";
        animator = gameObject.AddComponent<Animation>();
        animator.AddClip(idleClip, idleClip.name);
        animator.AddClip(turnClip, turnClip.name);
        animator.AddClip(walkClip, walkClip.name);
        sword = GetComponentInChildren<Sword>();

        foreach (AttackZone atkZone in attackZones)
        {
            animator.AddClip(atkZone.attackClip.animationClip, atkZone.attackClip.animationClip.name);
        }
    }

    public void NewAttack()
    {
        List<AttackClip> admissibleClip = new List<AttackClip>();

        foreach (AttackZone atkZone in attackZones) {
            float sqrDistanceToPlayer = (atkZone.zone.ClosestPoint(player.position) - player.position).sqrMagnitude;
            if (sqrDistanceToPlayer <= attackRangeAcceptance * attackRangeAcceptance){
                admissibleClip.Add(atkZone.attackClip);
            }
        }

        AttackClip attackClip = new AttackClip();
        if (admissibleClip.Count != 0)
        {
            int index = Random.Range(0, admissibleClip.Count);
            attackClip = admissibleClip[index];
        }
        else {
            int index = Random.Range(0, attackZones.Length);
            attackClip = attackZones[index].attackClip;
        }
        currentAttack = attackClip.animationClip.name;
        animator.CrossFade(currentAttack, 0.2f);
        StartCoroutine(SwordCheck(attackClip.startAtk * attackClip.animationClip.length, attackClip.endAtk * attackClip.animationClip.length));
    }

    IEnumerator SwordCheck(float startTime, float endTime) {
        yield return new WaitForSeconds(startTime);
        checkSword = true;
        yield return new WaitForSeconds(endTime - startTime);
        checkSword = false;
    }

    public void Update()
    {
        if (checkSword) {
            if (sword.CheckForCollision()) {
                Debug.Log("Player hitted");
                GameManager.Instance.EnemyHitPlayer();
                checkSword = false;
                PlayerController.Instance.TakeDamage();
            }
        }
        if (currentAttack != "")
        {
            sword.WriteOldPointList();
        }
    }

    public void Idle() {
        animator.CrossFade(idleClip.name, idleCrossFade);
        currentAttack = "";
    }

    public bool IsAttacking()
    {
        return animator.IsPlaying(currentAttack);
    }

    public void Turn()
    {
        animator.CrossFade(turnClip.name, turnCrossFade);
    }

    public void Walk()
    {
        animator.CrossFade(walkClip.name, walkCrossFade);
    }

}
