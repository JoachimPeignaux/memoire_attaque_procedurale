using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixedAnimation : MonoBehaviour, AnimationInterface
{
    private Dictionary<string, (Vector3, Vector3)> startAndEndAtkPositions;
    private Dictionary<string, (Vector3, Vector3)> startAndEndClipPositions;
    Animation animator;

    [Space]
    [SerializeField] AnimationClip idleClip;
    [SerializeField] float idleCrossFade;
    [SerializeField] AnimationClip turnClip;
    [SerializeField] float turnCrossFade;
    [SerializeField] AnimationClip walkClip;
    [SerializeField] float walkCrossFade;

    [Header("Attacking")]
    [SerializeField] List<AttackClip> animationBank;
    Vector3 A, C;
    Sword sword;
    bool checkSword;
    string currentAttack;
    Transform player;

    [Header("Mixed IK")]
    [SerializeField] IK_Mixed ik;
    [SerializeField] Transform hand;
    [SerializeField] CurveTarget curveTarget;
    float startTime;
    float endTime;
    float animTime;
    float clipLenght;

    enum PHASE { NONE, OPENING, ATTACK, RETURN}
    PHASE currentPhase;

    void Awake()
    {
        currentPhase = PHASE.NONE;
        currentAttack = "";
        animator = gameObject.AddComponent<Animation>();
        InitStartEndAnims();
        animator.AddClip(idleClip, idleClip.name);
        animator.AddClip(turnClip, turnClip.name);
        animator.AddClip(walkClip, walkClip.name);
        sword = GetComponentInChildren<Sword>();
        checkSword = true;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void NewAttack()
    {
        //get 2points
        (Vector3,Vector3) startAndEnd = curveTarget.GetStartAndEndForNewAttack();
        A = startAndEnd.Item1;
        C = startAndEnd.Item2;
        string clipName = GetCloserAnim(startAndEnd.Item1, startAndEnd.Item2);
        Vector3 handPosFrame0 = transform.rotation * startAndEndClipPositions[clipName].Item1 + transform.position;
        curveTarget.SetAC(handPosFrame0, A);
        curveTarget.MakeAttackPassingByTarget(curveTarget.origin.forward.normalized * ((handPosFrame0 - A).magnitude/3f) + curveTarget.origin.position);
        animator.Play(clipName);
        animTime = 0;
        clipLenght = animator[clipName].length;
        AttackClip mixableAnim = animationBank.Find(o => o.animationClip.name == clipName);
        startTime = mixableAnim.startAtk * clipLenght;
        endTime = mixableAnim.endAtk * clipLenght;
        currentAttack = clipName;
        currentPhase = PHASE.OPENING;
        StartCoroutine(EnableCheckSword(startTime));
    }

    void Update()
    {
        if (currentPhase == PHASE.NONE)
            return;

        animTime += Time.deltaTime;
        if (animTime <= startTime)
        {
            curveTarget.SetPosition(animTime/startTime);
        }
        else if (animTime >= endTime && animTime <=clipLenght)
        {
            if (currentPhase == PHASE.ATTACK) { 
                Vector3 handPosFrameLast = transform.rotation * startAndEndClipPositions[currentAttack].Item2 + transform.position;
                curveTarget.SetAC(C, handPosFrameLast);
                curveTarget.MakeAttackPassingByTarget(curveTarget.origin.forward.normalized * ((handPosFrameLast - C).magnitude / 3f) + curveTarget.origin.position);
                currentPhase = PHASE.RETURN;
            }
            curveTarget.SetPosition(  1 - ( (clipLenght - animTime) / (clipLenght - endTime) )  );
        }
        else {
            if (currentPhase == PHASE.OPENING)
            {
                curveTarget.SetAC(A, C);
                curveTarget.MakeAttackPassingByTarget(player.position);
                currentPhase = PHASE.ATTACK;
            }            
            float t = ( animTime - startTime ) / ( endTime - startTime );
            curveTarget.SetPosition(t);
        } 
    }

    void LateUpdate() {
        if (IsAttacking())
        {
            float t = 0;
            if (animTime < startTime)
            {
                t = animTime / startTime;
            }
            else if (animTime > endTime)
            {
                t = Mathf.Max(0, (clipLenght - animTime) / (clipLenght - endTime));
                checkSword = false;
            }
            else
            {
                t = 1;
            }

            ik.ResolveIK(t);
            curveTarget.SetRotation();

            if (t == 1 && checkSword)
            {
                if (sword.CheckForCollision())
                {
                    checkSword = false;
                    PlayerController.Instance.TakeDamage();
                    GameManager.Instance.EnemyHitPlayer();
                }
            }
            sword.WriteOldPointList();
        }
    }

    public void Idle()
    {
        animator.CrossFade(idleClip.name, idleCrossFade);
    }

    public bool IsAttacking()
    {
        return animator.IsPlaying(currentAttack);
    }

    void InitStartEndAnims()
    {
        startAndEndAtkPositions = new Dictionary<string, (Vector3, Vector3)>();
        startAndEndClipPositions = new Dictionary<string, (Vector3, Vector3)>();
        foreach (AttackClip mixableAnim in animationBank)
        {
            animator.AddClip(mixableAnim.animationClip, mixableAnim.animationClip.name);

            mixableAnim.animationClip.SampleAnimation(this.gameObject, mixableAnim.animationClip.length * mixableAnim.startAtk);
            Vector3 startPos = hand.position - transform.position;
            startPos = Quaternion.Inverse(transform.rotation) * startPos;

            mixableAnim.animationClip.SampleAnimation(this.gameObject, mixableAnim.animationClip.length * mixableAnim.endAtk);
            Vector3 endPos = hand.position - transform.position;
            endPos = Quaternion.Inverse(transform.rotation) * endPos;

            startAndEndAtkPositions.Add(mixableAnim.animationClip.name, (startPos, endPos));


            mixableAnim.animationClip.SampleAnimation(this.gameObject, 0);
            startPos = hand.position - transform.position;
            startPos = Quaternion.Inverse(transform.rotation) * startPos;

            mixableAnim.animationClip.SampleAnimation(this.gameObject, mixableAnim.animationClip.length - 0.01f);
            endPos = hand.position - transform.position;
            endPos = Quaternion.Inverse(transform.rotation) * endPos;

            startAndEndClipPositions.Add(mixableAnim.animationClip.name, (startPos, endPos));

        }
    }

    string GetCloserAnim(Vector3 start, Vector3 end)
    {
        float minDelta = float.MaxValue;
        string closestAnim = "";

        foreach (var anim in startAndEndAtkPositions)
        {
            float delta = 0;
            delta += (start - (transform.rotation * anim.Value.Item1 + transform.position)).sqrMagnitude;
            delta += (end - (transform.rotation * anim.Value.Item2 + transform.position)).sqrMagnitude;

            if (delta < minDelta)
            {
                minDelta = delta;
                closestAnim = anim.Key;
            }
        }

        return closestAnim;
    }

    public void Turn()
    {
        animator.CrossFade(turnClip.name, turnCrossFade);
    }

    public void Walk()
    {
        animator.CrossFade(walkClip.name, walkCrossFade);
    }

    IEnumerator EnableCheckSword(float start) {
        yield return new WaitForSeconds(start);
        checkSword = true;
    }
}