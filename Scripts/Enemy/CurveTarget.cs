using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveTarget : MonoBehaviour
{
    public Transform origin;
    [SerializeField] float radius;
    [SerializeField] List<CircleArc> swordOriginArcs;
    [SerializeField] Transform hand;
    [SerializeField] Transform sword;

    //B�zier
    Vector3 A, B, C;
    float closeT;
    Quaternion startQuaternion;

    private void Awake()
    {
        startQuaternion = hand.rotation;
    }

    public (Vector3, Vector3) GetStartAndEndForNewAttack() {

        int iArc = Random.Range(0, swordOriginArcs.Count);

        float angle = Random.Range(swordOriginArcs[iArc].endAngle, swordOriginArcs[iArc].startAngle);

        Vector3 a = origin.position;
        a += origin.rotation * new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad), 0);
        Vector3 c = origin.position;
        c += origin.rotation * new Vector3(Mathf.Cos((angle + 180) * Mathf.Deg2Rad), Mathf.Sin((angle + 180) * Mathf.Deg2Rad), 0);

        return (a, c);
    }

    public void SetAC(Vector3 a, Vector3 c) {
        A = a;
        C = c;
    }

    public void MakeAttackPassingByTarget(Vector3 target) {
        Vector3 targetProjectionOnSphere = (target - origin.position).normalized * radius*2 + origin.position;

        //calcule B pour que la courbe passe sur la cible
        float minDistance = (targetProjectionOnSphere - A).sqrMagnitude;
        closeT = 0f;

        for (float t = 0; t <= 1; t += 0.01f)
        {
            Vector3 P = BezierCurve(A, targetProjectionOnSphere, C, t);
            float distance = (targetProjectionOnSphere - P).sqrMagnitude;

            if (distance < minDistance)
            {
                minDistance = distance;
                closeT = t;
            }
        }

        B = (targetProjectionOnSphere - (1 - closeT) * (1 - closeT) * A - closeT * closeT * C)
                                        /
                                (2 * (1 - closeT) * closeT);
    }

    public void SetPosition(float t) {
        transform.position = BezierCurve(A, B, C, t);
    }

    public void SetRotation() {
        Vector3 swordDir = sword.position - hand.position;
        Vector3 lookAt = hand.position - hand.parent.position;
        hand.rotation = Quaternion.FromToRotation( swordDir, lookAt) * hand.rotation;
    }

    private void OnDrawGizmos()
    {
        //circle
        Gizmos.color = Color.magenta;
        Vector3 previousPoint = new Vector3();
        foreach (CircleArc arc in swordOriginArcs)
        {
            for (float i = arc.startAngle; i <= arc.endAngle; i += 3)
            {
                Vector3 point = origin.position;
                point += origin.rotation * new Vector3(Mathf.Cos(i * Mathf.Deg2Rad), Mathf.Sin(i * Mathf.Deg2Rad), 0);
                if (i != arc.startAngle)
                {
                    Gizmos.DrawLine(point, previousPoint);
                }
                previousPoint = point;
            }
        }

        //sword dir
        Gizmos.color = Color.red;
        Vector3 swordDir = sword.position - hand.position;
        Vector3 lookAt = hand.position - hand.parent.position;
        Gizmos.DrawLine(hand.position, hand.position + Quaternion.FromToRotation(swordDir, lookAt) * swordDir);

        //curve
        if (B != Vector3.zero)
        {
            Gizmos.color = Color.green;
            Vector3 previousPointCurve = new Vector3();
            for (float t = 0; t <= 1; t += 0.1f)
            {
                Vector3 point = BezierCurve(A, B, C, t);
                if (t != 0f)
                    Gizmos.DrawLine(point, previousPointCurve);
                previousPointCurve = point;
            }
        }
    }

    Vector3 BezierCurve(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        return (1 - t) * (1 - t) * a + 2 * (1 - t) * t * b + t * t * c;
    }
}

[System.Serializable]
struct CircleArc{
    public float startAngle;
    public float endAngle;
}
