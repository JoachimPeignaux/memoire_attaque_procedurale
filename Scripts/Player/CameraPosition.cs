using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    [SerializeField] Transform playerHead;
    [SerializeField] Transform enemyCenter;
    [SerializeField] float distanceToPlayer;

    void Update()
    {
        Vector3 dir = playerHead.position - enemyCenter.position;
        dir.y = 0;
        Vector3 targetPos = enemyCenter.position + dir + dir.normalized * distanceToPlayer;
        float v_angle = Vector3.Angle(playerHead.position - enemyCenter.position, targetPos - enemyCenter.position);
        float opposedLenght = Mathf.Sin(v_angle * Mathf.Deg2Rad) * (dir.magnitude + distanceToPlayer);
        targetPos += new Vector3(0, opposedLenght, 0);

        Quaternion LookEnemyRotation = Quaternion.LookRotation(enemyCenter.position - transform.position, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, LookEnemyRotation, Time.deltaTime * 10);
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime*10);
    }
}
