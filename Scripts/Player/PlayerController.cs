using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Attack")]
    [SerializeField] float atkDuration = 1f;
    [SerializeField] Transform swordHolder;
    EnemyBrain enemy;
    float atkProgess;
    float restAngle = 180;
    float endAngle = -90;
    Sword sword;
    bool checkSword = false;

    [Header("Dash")]
    [SerializeField] float dashDuration = 1f;
    [SerializeField] Vector3 dashLocalScale;
    Vector3 startScale;
    float dashProgress;
    [SerializeField] float dashDistance;
    Vector3 dashStart;
    Vector3 dashEnd;
    CapsuleCollider capsule;
    int blockingLayer;

    [Header("Movement")]
    Transform lookAt;
    [SerializeField] float speed;
    private bool isHitted;

    private Color startColor;

    [Space]
    public float hittedDuration;
    static public PlayerController Instance;

    private void Awake()
    {
        startScale = transform.localScale;
        sword = GetComponentInChildren<Sword>();
        isHitted = false;
        Instance = this;
        lookAt = GameObject.FindGameObjectWithTag("Enemy").transform;
        enemy = lookAt.GetComponent<EnemyBrain>();
        capsule = GetComponent<CapsuleCollider>();
        blockingLayer = LayerMask.GetMask("Enemy");
        startColor = GetComponent<MeshRenderer>().material.color;
    }

    void Update()
    {
        //si attaque, rien
        if (atkProgess != 0)
        {
            atkProgess += Time.deltaTime;
            swordHolder.localRotation = Quaternion.Euler(0,Mathf.Lerp(restAngle, endAngle, atkProgess / atkDuration),0);
            if (atkProgess > atkDuration || isHitted)
            {
                atkProgess = 0;
                swordHolder.localRotation = Quaternion.Euler(0, restAngle, 0);
            }
            if (checkSword && sword.CheckForCollision()) {
                Debug.Log("Enemy hitted");
                enemy?.Hitted();
                GameManager.Instance.PlayerHitEnemy();
                checkSword = false;
            }
            sword.WriteOldPointList();
        }
        else if (Input.GetMouseButtonDown(0) && atkProgess == 0 && !isHitted && dashProgress == 0)
        {
            atkProgess = 0.005f;
            checkSword = true;
            sword.WriteOldPointList();
        }
        else if (dashProgress != 0)
        {
            dashProgress += Time.deltaTime;
            float t = dashProgress / dashDuration;
            Vector3 targetPos = Vector3.Lerp(dashStart, dashEnd, t );
            if (!Physics.CheckSphere(targetPos, capsule.radius/4f, blockingLayer))
            {
                transform.position = targetPos;
                float tScale;
                if (t < 0.5f)
                {
                    tScale = t * 2;
                }
                else
                {
                    tScale = 2 * (1 - t);
                }

                transform.localScale = Vector3.Lerp(startScale, dashLocalScale, tScale);
            }
            else {
                dashProgress = dashDuration;
            }

            if (dashProgress >= dashDuration || isHitted)
            {
                dashProgress = 0;
                transform.localScale = startScale;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space) && dashProgress == 0 && !isHitted)
        {
            dashProgress = 0.005f;
            dashStart = transform.position;
            Vector3 dashDirection = GetInputDirection(transform);
            dashEnd = transform.position + dashDirection.normalized * dashDistance;
        }
        else if(!isHitted){
            transform.position += GetInputDirection(transform) * speed * Time.deltaTime;
        }

        Vector3 targetSameY = new Vector3(lookAt.position.x, transform.position.y, lookAt.position.z);
        transform.LookAt(targetSameY, Vector3.up);
    }

    private Vector3 GetInputDirection(Transform relativeTo)
    {
        Vector3 direction = Vector3.zero;
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W))
        {
            direction += relativeTo.forward;
        }

        if (Input.GetKey(KeyCode.S))
        {
            direction -= relativeTo.forward;
        }

        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.A))
        {
            direction -= relativeTo.right;
        }

        if (Input.GetKey(KeyCode.D))
        {
            direction += relativeTo.right;
        }

        return direction;
    }

    public void TakeDamage() {
        isHitted = true;
        StopAllCoroutines();
        StartCoroutine(ColorChange(hittedDuration));
        StartCoroutine(DisableIsHitted(hittedDuration));
    }

    IEnumerator ColorChange(float duration) {
        GetComponent<MeshRenderer>().material.color = Color.red;
        yield return new WaitForSeconds(duration);
        GetComponent<MeshRenderer>().material.color = startColor;
    }

    IEnumerator DisableIsHitted(float duration)
    {
        yield return new WaitForSeconds(duration);
        isHitted = false;
    }
}
