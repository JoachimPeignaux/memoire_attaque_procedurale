using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] string classicScene;
    [SerializeField] string mixedScene;
    [SerializeField] float timePerScene;
    [SerializeField] GameObject endUI;

    bool startWithClassic;
    string activeScene;

    ScoreManager scoreManager;

    //stats for nerds
    int playerHitClassic; //stat A
    int playerHitMixed;   //stat B

    int classicHitPlayer; //stat C
    int mixedHitPlayer;   //stat D

    static public GameManager Instance;

    private void Awake()
    {
        startWithClassic = true;
        DontDestroyOnLoad(this.gameObject);
        Instance = this;
        playerHitClassic = 0;
        playerHitMixed = 0;
        classicHitPlayer = 0;
        mixedHitPlayer = 0;
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void Start()
    {
        StartCoroutine(MainLoop());
    }

    IEnumerator MainLoop() {
        yield return new WaitUntil(() => Input.GetKey(KeyCode.M));
        scoreManager.HideTutorialText();
        scoreManager.ResetScore();

        if (startWithClassic)
        {
            activeScene = classicScene;
        }
        else {
            activeScene = mixedScene;
        }
        SceneManager.LoadScene(activeScene);
        yield return new WaitForSeconds(timePerScene);

        if (!startWithClassic)
        {
            activeScene = classicScene;
        }
        else
        {
            activeScene = mixedScene;
        }
        SceneManager.LoadScene(activeScene);
        yield return new WaitForSeconds(timePerScene);

        EndScreen endScreen = GameObject.Instantiate(endUI).GetComponent<EndScreen>();
        endScreen.A.text += playerHitClassic;
        endScreen.B.text += playerHitMixed;
        endScreen.C.text += classicHitPlayer;
        endScreen.D.text += mixedHitPlayer;
        Time.timeScale = 0.0f;
    }

    public void PlayerHitEnemy()
    {
        if (activeScene == classicScene)
        {
            playerHitClassic++;
        }
        else if (activeScene == mixedScene)
        {
            playerHitMixed++;
        }
        scoreManager.ChangeScore(true);
    }

    public void EnemyHitPlayer()
    {
        if (activeScene == classicScene)
        {
            classicHitPlayer++;
        }
        else if (activeScene == mixedScene)
        {
            mixedHitPlayer++;
        }
        scoreManager.ChangeScore(false);
    }
}
